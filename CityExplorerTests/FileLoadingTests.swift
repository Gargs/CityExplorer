//
//  FileLoadingTests.swift
//  CityExplorerTests
//
//  Created by Saurabh Garg on 08/06/2018.
//  Copyright © 2018 Saurabh Garg. All rights reserved.
//

import XCTest
@testable import CityExplorer

class FileLoadingTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testCitiesFileExists() {
        let path = citiesFilePath()
        XCTAssertNotNil(path, "The cities.json file was not included in the project")
    }
    
    func testFileHasValidContent() {
        let fileData = citiesFileData()
        XCTAssertNotNil(fileData, "The cities.json file is corrupt")
        let json = citiesJSON()
        XCTAssertNotNil(json, "The data cannot be converted to JSON")
    }
}
