//
//  ModelTests.swift
//  CityExplorerTests
//
//  Created by Saurabh Garg on 08/06/2018.
//  Copyright © 2018 Saurabh Garg. All rights reserved.
//

import XCTest
@testable import CityExplorer

class ModelTests: XCTestCase {
    let cities = CitySections(with: citiesFileData()!)?.reduce([], +)
    
    func testModelDecoding() {
        XCTAssertNotNil(cities, "Could not decode the JSON")
    }
    
    func testModelSorting() {
        if let cities = cities {
            let firstCity = cities.first!
            let lastCity = cities.last!
            XCTAssert(firstCity.name < lastCity.name, "Cities are not sorted alphabetically")
        } else {
            XCTFail("Failed to reduce the model to a 1D array")
        }
    }
    
    func testCityEquality() {
        if let cities = cities {
            let firstCity = cities.first!
            let lastCity = cities.last!
            XCTAssertNotEqual(firstCity, lastCity, "Equality failed")
            XCTAssertEqual(firstCity, firstCity, "Equality failed")
        } else {
            XCTFail("Failed to reduce the model to a 1D array")
        }
    }
    
    func testDiacriticsSorting() {
        if let cities = cities {
            let OrlandNO = cities.index { return $0.name == "Ørland" }!
            let OrlandUS = cities.index { return $0.name == "Orland" }!
            XCTAssert(OrlandNO > OrlandUS, "Cities are not sorted correctly wrt diacritics")
        } else {
            XCTFail("Failed to reduce the model to a 1D array")
        }
    }
    
    func testCapitalizationSorting() {
        if let cities = cities {
            let mountAlbert = cities.index { return $0.name == "MOUNT ALBERT" }!
            let mountAiry = cities.index { return $0.name == "Mount Airy" }!
            XCTAssert(mountAlbert > mountAiry, "Cities are not sorted correctly wrt case")
        } else {
            XCTFail("Failed to reduce the model to a 1D array")
        }
    }
}
