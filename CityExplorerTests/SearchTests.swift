//
//  SearchTests.swift
//  CityExplorerTests
//
//  Created by Saurabh Garg on 10/06/2018.
//  Copyright © 2018 Saurabh Garg. All rights reserved.
//

import XCTest
@testable import CityExplorer

class SearchTests: XCTestCase {
    let cities = CitySections(with: citiesFileData()!)!
    
    func testSingleCharacterCaseSensitiveSearch() {
        let search = "A"
        let searchResult = cities.search(forPrefix: search)
        let searchResultPrefixes = searchResult.map { String($0.name.prefix(1)) }
        XCTAssert(searchResultPrefixes.contains("A"), "Search did not contain lowercase results")
    }
    
    func testSingleCharacterCaseInsensitiveSearch() {
        let search = "A"
        let searchResult = cities.search(forPrefix: search)
        let searchResultPrefixes = searchResult.map { String($0.name.prefix(1)) }
        XCTAssert(searchResultPrefixes.contains("a"), "Search did not contain lowercase results")
    }
    
    func testSpacePrefixedSearch() {
        let search = "  A"
        let searchResult = cities.search(forPrefix: search)
        let searchResultPrexies = searchResult.map { String($0.name.prefix(1)) }
        XCTAssert(searchResultPrexies.contains("a"), "Search did not ignore leading whitespaces")
    }
    
    func testSpaceSuffixedSearch() {
        let search = "Amsterdam "
        let searchResult = cities.search(forPrefix: search)
        XCTAssert(searchResult.contains{ $0.name == "Amsterdam"  }, "Search did not ignore trailing whitespaces")
    }
    
    func testSearchAmsterdam() {
        let search = "amsterdam"
        let searchResult = cities.search(forPrefix: search)
        XCTAssert(searchResult.contains { $0.name == "Amsterdam" }, "Search results did not include Amsterdam")
    }
    
    func testSearchSanFrancisco() {
        let search = "San Francisco"
        let searchResult = cities.search(forPrefix: search)
        XCTAssert(searchResult.contains { $0.name == "San Francisco" }, "Search results did not include San Francisco")
    }
    
    func testDiacriticSearch() {
        let search = "Sao"
        let searchResult = cities.search(forPrefix: search)
        XCTAssert(searchResult.contains { $0.name == "São Benedito" }, "Search results did not include São Benedito")
    }
    
    func testForcedDiacriticSearch() {
        let search = "São"
        let searchResult = cities.search(forPrefix: search)
        XCTAssert(searchResult.contains { $0.name == "São Benedito" }, "Search results did not include São Benedito when searched for 'São'")
    }
    
    /*
    func testSearchSpeedWithDiacriticsPerformance() {
        let search = "Sao"
        measure {
            _ = cities.search(forPrefix: search)
        }
    }
 */
}
