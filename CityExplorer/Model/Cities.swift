//
//  Cities.swift
//  CityExplorer
//
//  Created by Saurabh Garg on 08/06/2018.
//  Copyright © 2018 Saurabh Garg. All rights reserved.
//

import MapKit

extension CLLocationCoordinate2D: Decodable {
    private enum CodingKeys: String, CodingKey {
        case lon
        case lat
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        let longitude = try values.decode(CLLocationDegrees.self, forKey: .lon)
        let latitude = try values.decode(CLLocationDegrees.self, forKey: .lat)
        self = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
}

struct City: Decodable, Equatable {
    let name: String
    let country: String
    let id: Int
    let coordinates: CLLocationCoordinate2D
    
    private enum CodingKeys: String, CodingKey {
        case name
        case country
        case id = "_id"
        case coordinates = "coord"
    }
    
    func description() -> String {
        return name + ", " + country
    }
    
    static func == (lhs: City, rhs: City) -> Bool {
        return lhs.id == rhs.id
    }
}

typealias Cities = [City]
typealias CitySections = [Cities]

private struct JSONModel: Decodable {
    private let cities: Cities
    let sections: CitySections
    
    init(from decoder: Decoder) throws {
        var raw = try decoder.unkeyedContainer()
        var cities = Cities()
        while !raw.isAtEnd {
            let city = try raw.decode(City.self)
            cities.append(city)
        }
        self.cities = cities.sorted { $0.name.compare($1.name, options: [.caseInsensitive, .diacriticInsensitive], range: nil, locale: nil) == .orderedAscending  }
        
        // The cities are sorted into sections based on the current locale's indexedcollation. This makes the search much faster as the first character is used to find the section, with subsequent keypresses only searching within that section. This drastically reduces the search time even though the complexity remains more or less the same as the number of elements to be searched is reduced.
        
        let collation = UILocalizedIndexedCollation.current()
        let sectionTitles = collation.sectionTitles
        var sections = Array(repeating: [], count: collation.sectionTitles.count) as [[City]]
        
        for city in self.cities {
            let prefix = String(city.name[city.name.startIndex])
            let sectionPredicate = NSPredicate(format: "self beginswith[cd] %@", prefix)
            if let sectionIndex = sectionTitles.index(where: { (title) -> Bool in
                return sectionPredicate.evaluate(with: title)
            }) {
                sections[sectionIndex].append(city)
            } else {
                sections[collation.sectionTitles.count - 1].append(city)
            }
        }
        self.sections = sections
    }
}

extension Array where Element == Cities {
    init?(with data: Data) {
        guard let instance = try? JSONDecoder().decode(JSONModel.self, from: data).sections else { return nil }
        self = instance
    }
    
    func search(forPrefix prefix: String) -> Cities {
        let prefix = prefix.trimmingCharacters(in: .whitespacesAndNewlines)
        guard !prefix.isEmpty else { return self.reduce([], +) }
        let searchFirstCharacter = String(prefix[prefix.startIndex])
        
        let sectionPredicate = NSPredicate(format: "self beginswith[cd] %@", searchFirstCharacter)
        let citySearchPredicate = NSPredicate(format: "self beginswith[cd] %@", prefix)
        if let sectionIndex = UILocalizedIndexedCollation.current().sectionTitles.index(where: { (title) -> Bool in
            return sectionPredicate.evaluate(with: title)
        }) {
            return self[sectionIndex].filter { citySearchPredicate.evaluate(with: $0.name) }
        } else {
            return self[UILocalizedIndexedCollation.current().sectionTitles.count - 1].filter { citySearchPredicate.evaluate(with: $0.name) }
        }
    }
}
