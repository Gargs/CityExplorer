//
//  CitiesLoader.swift
//  CityExplorer
//
//  Created by Saurabh Garg on 08/06/2018.
//  Copyright © 2018 Saurabh Garg. All rights reserved.
//

import Foundation

internal func citiesFilePath() -> URL? {
    guard let path = Bundle.main.path(forResource: "cities", ofType: "json") else { return nil }
    return URL(fileURLWithPath: path)
}

internal func citiesFileData() -> Data? {
    guard let path = citiesFilePath() else { return nil }
    return try? Data(contentsOf: path)
}

internal func citiesJSON() -> String? {
    guard let data = citiesFileData() else { return nil }
    return String(data: data, encoding: .utf8)
}
