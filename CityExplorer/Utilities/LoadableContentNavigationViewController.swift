//
//  LoadableContentNavigationViewController.swift
//  CityExplorer
//
//  Created by Saurabh Garg on 08/06/2018.
//  Copyright © 2018 Saurabh Garg. All rights reserved.
//

import UIKit

extension UINavigationController {
    
    private static let LoadingIndicatorTag = 0987654321
    
    var isLoadingIndicatorVisible: Bool {
        get {
            return isShowingLoadingIndicator()
        }
        set {
            newValue ? showLoadingIndicator() : removeLoadingIndicator()
        }
    }
    
    private func showLoadingIndicator() {
        guard let topViewControllerView = topViewController?.view else { return }
        guard isShowingLoadingIndicator() == false else { return }
        let backgroundView = UIView(frame: topViewControllerView.bounds)
        backgroundView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        backgroundView.tag = UINavigationController.LoadingIndicatorTag
        backgroundView.backgroundColor = UIColor.white
        let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        backgroundView.addSubview(activityIndicator)
        activityIndicator.center = backgroundView.center
        activityIndicator.frame = activityIndicator.frame.integral
        activityIndicator.autoresizingMask = [.flexibleLeftMargin, .flexibleRightMargin]
        let label = UILabel()
        label.autoresizingMask = [.flexibleLeftMargin, .flexibleRightMargin]
        label.font = UIFont.preferredFont(forTextStyle: .subheadline)
        label.textColor = UIColor.darkGray
        label.text = "Loading"
        label.sizeToFit()
        label.center = CGPoint(x: activityIndicator.center.x, y: activityIndicator.center.y + activityIndicator.bounds.size.height + 10)
        backgroundView.addSubview(label)
        topViewControllerView.addSubview(backgroundView)
        topViewControllerView.bringSubview(toFront: backgroundView)
        activityIndicator.startAnimating()
    }
    
    private func removeLoadingIndicator() {
        if let loadingIndicatorView = view.viewWithTag(UINavigationController.LoadingIndicatorTag) {
            loadingIndicatorView.tag = 0
            UIView.animate(withDuration: 0.1,
                           animations: {
                            loadingIndicatorView.alpha = 0 },
                           completion: { (success) in
                            if (success) {
                                loadingIndicatorView.removeFromSuperview()
                            }
            })
        }
    }
    
    private func isShowingLoadingIndicator() -> Bool {
        return (view.viewWithTag(UINavigationController.LoadingIndicatorTag) == nil) ? false : true
    }
}
