//
//  CitiesSearchManager.swift
//  CityExplorer
//
//  Created by Saurabh Garg on 08/06/2018.
//  Copyright © 2018 Saurabh Garg. All rights reserved.
//

import UIKit

extension CitiesTableViewController: UISearchResultsUpdating, UISearchControllerDelegate, UISearchBarDelegate {
    func setupSearching() {
        guard let searchResultsViewController = storyboard?.instantiateViewController(withIdentifier: "SearchResultsViewController") as? CitiesSearchResultsViewController else { return }
        searchController = UISearchController(searchResultsController: searchResultsViewController)
        searchController.delegate = self
        searchController.searchResultsUpdater = self
        
        if #available(iOS 11.0, *) {
            navigationItem.searchController = searchController
            navigationItem.hidesSearchBarWhenScrolling = false
        } else {
            tableView.tableHeaderView = searchController.searchBar
        }
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        guard let searchTerm = searchController.searchBar.text else { return }
        searchOperationQueue.cancelAllOperations()
        searchOperationQueue.addOperation { [unowned self] in
            let results = self.sections?.search(forPrefix: searchTerm)
            OperationQueue.main.addOperation {
                (searchController.searchResultsController as! CitiesSearchResultsViewController).searchResults = results
            }
        }
    }
    
    func didDismissSearchController(_ searchController: UISearchController) {
        searchOperationQueue.cancelAllOperations()
    }
}
