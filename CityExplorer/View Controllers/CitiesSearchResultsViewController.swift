//
//  CitiesSearchResultsViewController.swift
//  CityExplorer
//
//  Created by Saurabh Garg on 11/06/2018.
//  Copyright © 2018 Saurabh Garg. All rights reserved.
//

import UIKit

class CitiesSearchResultsViewController: UITableViewController {
    var searchResults: Cities? {
        didSet {
            tableView.reloadData()
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResults?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        let city = searchResults![indexPath.row]
        cell.textLabel?.text = city.description()
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presentingViewController?.performSegue(withIdentifier: "showDetail", sender: self)
    }
}
