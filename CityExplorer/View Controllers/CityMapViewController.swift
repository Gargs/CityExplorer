//
//  CityMapViewController.swift
//  CityExplorer
//
//  Created by Saurabh Garg on 08/06/2018.
//  Copyright © 2018 Saurabh Garg. All rights reserved.
//

import MapKit

private let MapRegionDistance = 30000.0 //Set the region to stretch 30kms out in both directions from center

class CityMapViewController: UIViewController {
    @IBOutlet private weak var mapView: MKMapView!
    var city: City? {
        didSet {
            configureView()
        }
    }

    func configureView() {
        if let city = city, isViewLoaded {
            title = city.description()
            mapView.removeAnnotations(mapView.annotations)
            let coordinates = city.coordinates
            mapView.centerCoordinate = coordinates
            
            let region = MKCoordinateRegionMakeWithDistance(coordinates, MapRegionDistance, MapRegionDistance)
            mapView.setRegion(mapView.regionThatFits(region), animated: true)
            
            let annotation = MKPointAnnotation()
            annotation.coordinate = coordinates
            mapView.addAnnotation(annotation)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
}

