//
//  CitiesTableViewController.swift
//  CityExplorer
//
//  Created by Saurabh Garg on 08/06/2018.
//  Copyright © 2018 Saurabh Garg. All rights reserved.
//

import UIKit

class CitiesTableViewController: UITableViewController {
    var searchController: UISearchController!
    var sections: CitySections? {
        didSet {
            updateLoadingIndicator()
            tableView.reloadData()
            tableView.reloadSectionIndexTitles()
        }
    }
    let searchOperationQueue: OperationQueue = {
        let queue = OperationQueue()
        queue.qualityOfService = .userInitiated
        queue.name = "City Search Queue"
        queue.maxConcurrentOperationCount = 1
        return queue
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupSearching()
        loadContent()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        updateLoadingIndicator()
    }

    // MARK: - Segues
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == "showDetail" else { return }
        var city: City?
        if let resultsTableViewController = sender as? CitiesSearchResultsViewController, let indexPath = resultsTableViewController.tableView.indexPathForSelectedRow {
            city = resultsTableViewController.searchResults?[indexPath.row]
        } else if let indexPath = tableView.indexPathForSelectedRow {
            city = sections?[indexPath.section][indexPath.row]
        }
        guard city != nil else { return }
        let controller = (segue.destination as! UINavigationController).topViewController as! CityMapViewController
        controller.city = city!
        controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
        controller.navigationItem.leftItemsSupplementBackButton = true
    }

    // MARK: - Table View
    
    override func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return sections == nil ? nil :  UILocalizedIndexedCollation.current().sectionIndexTitles
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return sections?.count ?? 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections?[section].count ?? 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        let city = sections![indexPath.section][indexPath.row]
        cell.textLabel!.text = city.description()
        return cell
    }
    
    // MARK: - Private functions
    
    private func loadContent() {
        DispatchQueue.global(qos: .userInitiated).async {
            guard let data = citiesFileData() else { return }
            let sections = CitySections(with: data)
            DispatchQueue.main.async { [unowned self] in
                self.sections = sections
            }
        }
    }
    
    private func updateLoadingIndicator() {
        navigationController?.isLoadingIndicatorVisible = (sections == nil)
    }
}
